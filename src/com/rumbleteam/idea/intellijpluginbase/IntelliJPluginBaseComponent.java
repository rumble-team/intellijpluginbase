package com.rumbleteam.idea.intellijpluginbase;

import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

/**
 * @author Thorsten 'stepo' Hallwas <stepotronic@web.de>
 */
public class IntelliJPluginBaseComponent implements ProjectComponent {

    final Project project;

    public IntelliJPluginBaseComponent(Project project) {
        this.project = project;
    }

    @NotNull
    @Override
    public String getComponentName() {
        return "IntelliJPluginBase";
    }

    @Override
    public void projectOpened() {

    }

    @Override
    public void projectClosed() {

    }

    @Override
    public void initComponent() {

    }

    @Override
    public void disposeComponent() {

    }
}
